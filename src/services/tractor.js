import axios from "axios/index";

class TractorService {

    getTractors(params) {
        return axios.get(process.env.REACT_APP_BASE_API_PATH + '/tractors', {params: params});
    }

}

export default (new TractorService());