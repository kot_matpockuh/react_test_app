import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Link} from "react-router-dom";
import './App.css';
import TractorList from './components/tractors/tractorsList';

class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    <nav className="navbar navbar-dark bg-dark">
                        <a className="navbar-brand" href="/">Cargo2</a>
                    </nav>
                    <div className="container-fluid sidebar-and-content">
                        <div className="row">
                            <nav className="col-2 hidden-xs-down bg-dark sidebar">
                                <ul>
                                    <li>
                                        <Link to="/tractors">Tractors</Link>
                                    </li>
                                </ul>
                            </nav>

                            <main className="main-content col-10 offset-2">
                                <div className="h-100 pt-4 pb-4">
                                    <Route path="/tractors" component={TractorList}/>
                                </div>
                            </main>
                        </div>
                    </div>
                </div>
            </Router>
        );
    }
}

export default App;
