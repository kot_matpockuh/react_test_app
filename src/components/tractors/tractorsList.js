import React, {Component} from 'react';

import TractorService from '../../services/tractor';
import PaginationBar from '../../shared/pagination/pagination';
import TableHeader from '../../shared/tableHeader/tableHeader';
import {GlobalConst} from '../../constants/global';
import * as qs from 'query-string';


class TractorList extends Component {

    // define state variables
    state = {
        currentPage: 0,
        pageParams: {
            offset: 0,
            limit: GlobalConst.defaultPageLim,
            sortField: undefined,
            sortOrder: undefined,
            search: undefined
        },
        tractors: {
            total: 0,
            data: []
        }
    };

    //define class variables
    isLoading = false;

    componentWillMount() {
        //parse URL-params (when somebody puts a link with params into browser)

        const parsedUrl = qs.parse(window.location.search);
        const objKeys = Object.keys(parsedUrl);

        const tempStateData = this.state.pageParams;
        for (const keyItem of objKeys) {
            if (this.state.pageParams.hasOwnProperty(keyItem)) {
                if (keyItem === 'offset' || keyItem === 'limit') {
                    tempStateData[keyItem] = Number(parsedUrl[keyItem] || 0);
                } else {
                    tempStateData[keyItem] = parsedUrl[keyItem];
                }
            }
        }

        this.setState({
            pageParams: tempStateData
        }, () => {
            // to be sure: state have updated already & update current pagination page

            if (this.state.pageParams.offset > 0) {
                this.setState({
                    currentPage: Math.ceil(this.state.pageParams.offset / this.state.pageParams.limit)
                });
            }
        });
    }

    componentDidMount() {
        // after all URL & state manipulations: get data from the server

        this.getTractors();
    }

    getTractors() {
        // here we use service

        this.clearTractorsList();

        this.isLoading = true;

        const data = this.state.pageParams;

        TractorService.getTractors(data)
            .finally(() => {
                this.isLoading = false;
            })
            .then(res => {
                this.setState({tractors: res.data});
            })
    }

    clearTractorsList() {
        // first of all clear existing list

        this.setState({
            tractors: {
                total: 0,
                data: []
            }
        });
    }

    changePage(pageNr, limit) {
        // get data from pagination component and transform it & change URL

        const offset = (pageNr) * limit > 0 ? (pageNr * limit) : 0;

        const statePageParams = this.state.pageParams;
        statePageParams.offset = offset;
        statePageParams.limit = limit;

        this.setState({
            currentPage: pageNr,
            pageParams: statePageParams
        });

        this.props.history.push({
            pathname: '/tractors',
            search: qs.stringify(this.state.pageParams)
        });

        this.getTractors();
    }

    setSorting(sortField, sortOrder) {
        // get data from table-header component and transform it & change URL

        const statePageParams = this.state.pageParams;
        statePageParams.offset = 0;
        statePageParams.sortField = sortField;
        statePageParams.sortOrder = sortOrder;

        this.setState({
            currentPage: 0,
            pageParams: statePageParams
        });

        this.props.history.push({
            pathname: '/tractors',
            search: qs.stringify(this.state.pageParams)
        });

        this.getTractors();
    }

    setSearchValue(val){
        // get data from search input, transform it

        const statePageParams = this.state.pageParams;
        statePageParams.offset = 0;
        statePageParams.search = val;

        this.setState({
            currentPage: 0,
            pageParams: statePageParams
        });
    }

    searchTractors(event) {
        // change URL & get data

        event.preventDefault();

        this.props.history.push({
            pathname: '/tractors',
            search: qs.stringify(this.state.pageParams)
        });

        this.getTractors();
    }

    checkIfFieldIsSorted(field) {
        // check: was field sorted or not?

        return this.state.pageParams.sortField === field;
    }

    checkWayOfSorting() {
        // check: was field sorted asc/desc?

        return this.state.pageParams.sortOrder === 'asc' ? true : false;
    }

    render() {
        return (
            <div>
                <form onSubmit={event => this.searchTractors(event)}>
                    <div className="input-group mb-3">
                        <input type="text" className="form-control" placeholder="Find..."
                               onChange={e => this.setSearchValue(e.target.value)}/>

                        <div className="input-group-append">
                            <button className="btn btn-outline-secondary" type="submit">⌕</button>
                        </div>
                    </div>
                </form>

                <div className="grid-table">
                    <div className="grid-table-header">
                        <div className="grid-table-header-col">
                            <TableHeader text={"Reg. Number"} field={"registrationNumber"}
                                         isActive={this.checkIfFieldIsSorted("registrationNumber")}
                                         sortOrder={this.checkWayOfSorting()}
                                         onSortingChanged={this.setSorting.bind(this)}></TableHeader>
                        </div>

                        <div className="grid-table-header-col">
                            <TableHeader text={"Manufacturer"} field={"manufacturer"}
                                         isActive={this.checkIfFieldIsSorted("manufacturer")}
                                         sortOrder={this.checkWayOfSorting()}
                                         onSortingChanged={this.setSorting.bind(this)}></TableHeader>
                        </div>

                        <div className="grid-table-header-col">
                            <TableHeader text={"Model"} field={"model"}
                                         isActive={this.checkIfFieldIsSorted("model")}
                                         sortOrder={this.checkWayOfSorting()}
                                         onSortingChanged={this.setSorting.bind(this)}></TableHeader>
                        </div>

                        <div className="grid-table-header-col">
                            <TableHeader text={"Actual Mileage"} field={"actualMileage"}
                                         isActive={this.checkIfFieldIsSorted("actualMileage")}
                                         sortOrder={this.checkWayOfSorting()}
                                         onSortingChanged={this.setSorting.bind(this)}></TableHeader>
                        </div>
                    </div>

                    <div className="grid-table-body">
                        {
                            this.state.tractors.data.map(tractor =>
                                <div className="grid-table-body-row" key={tractor._id}>
                                    <div className="grid-table-body-row-col">
                                        {tractor.registrationNumber}
                                    </div>

                                    <div className="grid-table-body-row-col">
                                        {tractor.manufacturer}
                                    </div>

                                    <div className="grid-table-body-row-col">
                                        {tractor.model}
                                    </div>

                                    <div className="grid-table-body-row-col">
                                        {tractor.actualMileage}
                                    </div>
                                </div>)
                        }
                    </div>
                </div>

                <PaginationBar total={this.state.tractors.total} current={this.state.currentPage}
                               pageSize={Number(this.state.pageParams.limit)}
                               onShowSizeChange={this.changePage.bind(this)}>
                </PaginationBar>

                {
                    this.isLoading && <span>Loading....</span>
                }
            </div>
        )
    }
}

export default TractorList;