import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './tableHeader.css';

class TableHeader extends Component {
    state = {
        sortOrder: true,
        isActive: false
    };

    static propTypes = {
        text: PropTypes.string,
        field: PropTypes.string,
        isActive: PropTypes.bool,
        sortOrder: PropTypes.bool,
        onSortingChanged: PropTypes.func
    };

    static defaultProps = {
        text: '',
        sortOrder: undefined
    };

    componentDidMount() {
        // init our data

        this.setState({
            isActive: this.props.isActive,
            sortOrder: !this.props.sortOrder
        });
    }

    onSorted = () => {
        // get&set our sorting: field & way of sorting

        this.setState({
            isActive: true,
            sortOrder: !this.state.sortOrder
        });

        let sorted;
        this.state.sortOrder ? sorted = 'asc' : sorted = 'desc';
        this.props.onSortingChanged(this.props.field, sorted);
    }

    renderArrow() {
        // render sorting arrow: asc/desc

        if (this.props.isActive) {
            return this.state.sortOrder ? <span>↓</span> : <span>↑</span>
        }
    }

    render() {
        return (
            <div className={"table-header"} onClick={this.onSorted}>
                <span>{this.props.text}</span>
                {this.renderArrow()}
            </div>
        )
    }
}

export default TableHeader;