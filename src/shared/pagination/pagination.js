import React, {Component} from 'react';
import {GlobalConst} from '../../constants/global';
import PropTypes from 'prop-types';
import ReactPaginate from 'react-paginate';
import './pagination.css';

class PaginationBar extends Component {
    static propTypes = {
        current: PropTypes.number,
        total: PropTypes.number,
        pageSize: PropTypes.number,
        onShowSizeChange: PropTypes.func
    };

    static defaultProps = {
        current: GlobalConst.defaultPageNr,
        total: 0,
        pageSize: GlobalConst.defaultPageLim
    };

    perPageOptions = [10, 25, 50, 100];

    constructor(props) {
        super(props);
        this.state = {
            pageSize: null
        }
    }

    componentDidMount() {
        // copy props to state, so we can use it (state) in this component

        const pageSize = this.props.pageSize;
        this.setState({
            pageSize: pageSize
        })
    }

    onShowSizeChange = (data) => {
        // when we change perPage drop-down

        this.setState({pageSize: data.target.value});
        this.props.onShowSizeChange(0, data.target.value);
    }

    onChange = (data) => {
        // when we change current page (pagination)

        this.props.onShowSizeChange(data.selected, this.props.pageSize);
    }

    render() {
        return (
            <nav className="d-flex justify-content-center position-relative pagination">
                <div className="pagination-per-page">
                    <div className="form-group row align-items-center">
                        <label htmlFor="perPageSelect" className="col-12 col-lg-6 pr-0 m-0">Per page:</label>
                        <div className="col-12 col-lg-6">
                            <select className="form-control" id="perPageSelect"
                                    onChange={this.onShowSizeChange}
                                    value={this.props.pageSize}>
                                {
                                    this.perPageOptions.map((option, index) =>
                                        <option key={index}>{option}</option>)
                                }
                            </select>
                        </div>
                    </div>
                </div>

                <ReactPaginate previousLabel={"«"}
                               nextLabel={"»"}
                               breakLabel={<a href="" className="page-link">...</a>}
                               breakClassName={"page-item disabled"}
                               initialPage={this.props.current}
                               forcePage={this.props.current}
                               pageCount={Math.ceil(this.props.total / this.state.pageSize)}
                               marginPagesDisplayed={2}
                               pageRangeDisplayed={4}
                               onPageChange={this.onChange}
                               disableInitialCallback={true}
                               containerClassName={"pagination"}
                               activeClassName={"active"}
                               pageClassName={"page-item"}
                               pageLinkClassName={"page-link"}
                               previousClassName={"page-item"}
                               nextClassName={"page-item"}
                               previousLinkClassName={"page-link"}
                               nextLinkClassName={"page-link"}/>
            </nav>
        )
    }
}

export default PaginationBar;